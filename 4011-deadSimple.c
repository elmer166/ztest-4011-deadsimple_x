/* \file 4011-deadSimple.c
 *
 *****************************************************************************/

//#include <p30Fxxxx.h>
#include <xc.h>

_FOSC( XT )                     // 7.3728 rock / 4 = 1.8MHz
_FWDT( WDT_OFF )                            // Watchdog timer off

int main (void)
{
  int i;

  _LATD1 = 1;
  _TRISD1 = 0;
  _TRISD2 = 0;
  for ( i=0; i<32766; i++ )
     ;
   _TRISD2 = 1;

  while (1)
    {
      _LATD1 = 1;
      for ( i=0; i<32766; i++ )
        ;
      _LATD1 = 0;
      for ( i=0; i<32766; i++ )
        ;
    }
}



